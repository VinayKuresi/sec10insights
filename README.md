# SEC10INSIGHTS

                                                       SEC 10K Insights Project.

Use Case:
Analyze the Sentimental impact SEC 10K report on the stock price movement around the filing date.
PipeLine Created upto cleaning the SEC 10K data, and doing sentiment analysis on the data.
Software List : 
Anaconda, Python/Jupyter Notebook, python 3.6, Numpy 1.16.2, Pandas 0.24.2, Scikit-klearn 0.20.3, 
BeautifulSoup4 4.6.3, nltk 3.4.
Hardware List :
OS                                          Microsoft Windows.
processor	Intel Core i3 (4th Gen) Processor
ram	4 GB DDR3 RAM
Resources find useful :
http://laurenhcohen.com/wp-content/uploads/2017/09/lazyprices.pdf.
Data PipeLine WorkFlow :
Data Processing:
Step 1 : Read the data from the Excel Sheet containing the data P&G 500 companies.
Step 2 : Extract the sec 10k content of the few companies in p&g 500 companies form www.SEC.gov.in. , from date 2014-01-01 to date 2016-01-01, due to computation power.
Function : download_and_extract_index(year_start ,year_end) :  downloads the data from the website related to the 10K report of the companies.
Data :  Form Type ,Company Name, CIK, Date, Filed, File Name. and store it into a csv file.
Step 3 : clean the extracted data using, BeautifulSoup.
Function : download_10k(): downloads the html content 10k data of the companies and  by parsing it using the beautifulSoup object, and writes the data to a flat text file.
Function : normalize_text(text) : Take care of breaklines & whitespaces combinations due to beautifulsoup parsing, find section, reformat item headers.
Step 4 : Parse the data related to the sections mentioned below : 
Function : extract_context(): calls the functions to extract the content related to below section and writes the data to flat text file.
•	MDA(Management’s Discussion and Analysis of Financial Condition and Results of Operations). 
Function : parse_mda(text, start=0): Define start & end signal for parsing, ITEM 7.

•	Risk Factors
Function : parse_rf(text, start=0): Define start & end signal for parsing, ITEM 1A.
•	Quantitative and Qualitative Disclosure about Market Risks
Function : parse_qq(text, start=0): Define start & end signal for parsing, ITEM 7A.
Getting Insights doing Sentimental Analysis : 
Repeated the data processing steps followed above in an simple and easier way, in few lines of code.
Function : get_sec_data(cik, doc_type):  pull a list of filled 10k’s from the SEC for each company.
Data :  File Name, Form Type, Date Filed, in a dictionary. 
Example : containing ticker key : value File Name, Form Type, Date Filed.
Sec_data[‘AMZN’] = [('https://www.sec.gov/Archives/edgar/data/1018724/000101872418000005/0001018724-18-000005-index.htm',
  '10-K',
  '2018-02-02'),
 ('https://www.sec.gov/Archives/edgar/data/1018724/000101872417000011/0001018724-17-000011-index.htm',
  '10-K',
  '2017-02-10')] value.
Here I had used only five Companies due to computation power, date range from 2014 to 2017 , It is a dictionary containing the data related to company Ticker key : CIK value.
Function : write_rar_data(): writes the uncleaned extracted data to a flat file, and copies data in a dictionary form.
Example : filling_documents_by_ticker, is the dictionary containing the data, in ‘ticker’ key : {‘date’ key : ‘raw data’ value}
filling_documents_by_ticker[‘AMZN’] = {
'2018-02-02' : raw data,
'2017-02-10': raw data
}
Function : Clean_text(text): calls the extract_content(text) function, which calls 
1.	Parse_qq
2.	Parse_mda
3.	Parse_rf,
And stores data in a dictionary form, and writes the clean text to file.
Example :  Ten_ks_by_ticker, is a dictionary containing data, ticker key : cik, raw data, file date values, as a list.


Ten_ks_by_ticker[‘AMZN’] = {

    cik: '0001018724'
    file: raw data
    file_date: '2018-02-02'},
  {
    cik: '0001018724'
    file: raw data
    file_date: '2017-02-10'

}

Function : lemmatize_words(words): lemmatization function to distil the verbs down, in the given words provided. It considers the context and converts the word to its meaningful base form.
Function : remove_stop_words(): A stop word is a commonly used word (such as “the”, “a”, “an”, “in”) that a search engine has been programmed to ignore.
Function : extract_LoughranMcDonald_MasterDictionary(sentiments): we will be using the Loughran and McDonald sentiment word lists. These word lists cover the following sentiments.
•	Negative
•	Positive
•	Uncertainty
•	Litigious
Where we have removed the unused information by extracting only rows containing atleast a true statement, by converting the digital information present in the sentiment_df table to Boolean. After that we had applied lemmatization on the given table, and removed the duplicate words.
Function : get_bag_of_words(sentiment_words, docs): using the sentiment word lists, we had generated a sentiment bag of words that counts the number of sentiment words in each doc, by ignoring the words not in sentiment_words.
Function : get_jaccard_similarity(bag_of_word_matrix): using the bag of words, we have calculated the jaccard similarity on the bag of words and plot it over time. By implement get_jaccard_similarity to return the jaccard similarities between each ticker in time. Since the input, bag_of_words_matrix, is a bag of words for each time period in order, we just need to compute the jaccard similarities for each neighbouring bag of words, by turning the bag of words into a Boolean array when calculating the jaccard similarity.
Function : get_tfidf(sentiment_words, docs): using the sentiment word lists, we have generated sentiment tfidf from the 10-k document, by implementing get_tfidf to generate tfidf from each document, using sentiment words as the terms, by ignoring words that are not in sentiment_words.
Function : get_cosine_similarity(tfidf_matrix): using the tfidf values, we will calculate the cosine similarities between each ticker in time. Since the input, tfidf_matrix is a tfidf vector for each time period in order, we need to compute the cosine similarities for each neighbouring vector.
